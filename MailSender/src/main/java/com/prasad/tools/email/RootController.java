package com.prasad.tools.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

	@Autowired
	private SMTPMailSender smptMailSender;
	
	@RequestMapping("/send-mail")
	public void sendMail()
	{
		
		smptMailSender.send("bamhanedeepli@gmail.com", "Test mail", "Test bdy");
	}
	
}
