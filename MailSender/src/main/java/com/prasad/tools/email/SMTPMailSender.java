package com.prasad.tools.email;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class SMTPMailSender {
	

	
	@Autowired
	private JavaMailSender javaMailSender;
	public void send(String to,String Subject,String body )
	{
		MimeMessage message =javaMailSender.createMimeMessage();
		MimeMessageHelper helper;
		
		try {
			helper=new MimeMessageHelper(message,true);
			helper.setSubject(Subject);
			helper.setTo(to);
			helper.setText(body);
			javaMailSender.send(message);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
